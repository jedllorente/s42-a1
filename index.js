// DOM - document object model
// const txtFirstName = document.querySelector('#txt-first-name')
// querySelctor function takes a string pinput that is formatted like css slector when applying styles. This allows to get a specific Element.
// we can contain code inside a constant.

/* alternative
document.getElementById('txt-first-name')
document.getElementsByClass()
document.getElementByTag() */

const spanFullName = document.querySelector('#span-full-name')
const txtFirstName = document.querySelector('#txt-first-name')
const txtLastName = document.querySelector('#txt-last-name')

const firstName = txtFirstName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = `${txtFirstName.value}`
})

txtLastName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
})

// console.log(span)